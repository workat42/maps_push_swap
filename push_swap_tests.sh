TIMEFORMAT=%R
PATH=./push_swap
echo "--------------------------------"
echo "\033[31;1mError checks\033[00;0m"
echo "--------------------------------"
echo "No args			: \c" && $PATH
echo "Alpha arg		: \c" && $PATH r
echo "Alpha args		: \c" && $PATH r d
echo "Alphanum (0)		: \c" && $PATH 4 r
echo "Alphanum (1)		: \c" && $PATH r 4
echo "Alphanum (2)		: \c" && $PATH 4 r 5
echo "Alphanum (3)		: \c" && $PATH 5r4
echo "Same int		: \c" && $PATH 5 5
echo "Bigger than int		: \c" && $PATH 2147483648
echo "Smaller than int	: \c" && $PATH -2147483649
echo "--------------------------------"

echo "\033[31;1mSpecial cases error\033[00;0m"
echo "--------------------------------"
echo "One arg			: \c" && $PATH 42
echo "Already ordered		: \c" && $PATH 1 2 3 4 5
echo "--------------------------------"

echo "\033[31;1mSmall checks\033[00;0m"
echo "--------------------------------"
echo "Max int (Should work)	: \c" && $PATH 2147483647 42 21
echo "Min int (Should work)	: \c" && $PATH -2147483648 42 21
echo "Mix 2 ints (0)		: \c" && $PATH 6 4
echo "Mix 3 ints (1)		: \c" && $PATH 1 7 4
echo "Mix 4 ints (2)		: \c" && $PATH 3 6 9 8
echo "Mix 5 ints (3)		: \c" && $PATH 5 4 3 2 1
echo "Mix 5 ints with - (0)	: \c" && $PATH -4 -6
echo "Mix 5 ints with - (1)	: \c" && $PATH 1 -4 7
echo "Mix 5 ints with - (2)	: \c" && $PATH -3 6 -9 8
echo "Mix 5 ints with - (3)	: \c" && $PATH 5 4 -3 -2 -1
echo "--------------------------------"

echo "\033[31;1mBig checks\033[00;0m"
echo "--------------------------------"
echo "2000 random numbers (< 10s is very good, 10s ~ 20s is not too bad, > 20s is bad) : "
time $PATH `ruby -e "puts (-1000..1000).to_a.shuffle.join(' ')"` | wc -w  | tr -d ' ' &
sleep 1
while pgrep "push_swap" > /dev/null; do
	ps -c | grep push_swap
	sleep 10
done
wait
echo "\n10000 numbers with one number not ordered (< 1mn is very good, 1mn ~ 5mn is not too bad, > 5mn is bad): "
time $PATH `ruby -e "puts (-5000..5000).to_a.reverse.insert(rand(8000) + 1000, 10001).join(' ')"` | wc -w  | tr -d ' ' &
sleep 1
while pgrep "push_swap" > /dev/null; do
	ps -c | grep push_swap
	sleep 10
done
wait
echo "--------------------------------"

echo "\033[31;1mAlgo small stack (part 1)\033[00;0m"
echo "--------------------------------"
echo "Bottom two numbers swaped (max 5 opes, 7 is ~ but can be ok) : \c"
result=$($PATH 0 1 2 3 4 5 6 7 9 8 | wc -w | tr -d ' ')
	if [ "$result" -le 5 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	elif [ "$result" -le 7 ] ; then
		echo "\033[33;1m~\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 0 1 2 3 4 5 6 7 9 8

echo "Bottom three numbers swaped (max 8 opes) : \c"
result=$($PATH 0 1 2 3 4 5 6 9 8 7 | wc -w | tr -d ' ')
	if [ "$result" -le 8 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 0 1 2 3 4 5 6 9 8 7

echo "Bottom four numbers swaped (max 10 opes) : \c"
result=$($PATH 0 1 2 3 4 5 9 8 7 6 | wc -w | tr -d ' ')
	if [ "$result" -le 10 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 0 1 2 3 4 5 9 8 7 6

echo "Top two numbers swaped (max 1 ope) : \c"
result=$($PATH 1 0 2 3 4 5 6 7 8 9 | wc -w | tr -d ' ')
	if [ "$result" -le 1 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 1 0 2 3 4 5 6 7 8 9

echo "Top three numbers swaped (max 5 ope) : \c"
result=$($PATH 2 1 0 3 4 5 6 7 8 9 | wc -w | tr -d ' ')
	if [ "$result" -le 5 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 2 1 0 3 4 5 6 7 8 9
echo "--------------------------------"

echo "\033[31;1mAlgo small stack (part 2 : rotate)\033[00;0m"
echo "--------------------------------"
echo "Just RA needed (max 1 ope) : \c"
result=$($PATH 10 1 2 3 4 5 6 7 8 9 | wc -w | tr -d ' ')
	if [ "$result" -le 1 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 10 1 2 3 4 5 6 7 8 9

echo "Just RA RA needed (max 2 ope): \c"
result=$($PATH 9 10 1 2 3 4 5 6 7 8 | wc -w | tr -d ' ')
	if [ "$result" -le 2 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 9 10 1 2 3 4 5 6 7 8

echo "Just RA RA RA needed (max 3 ope) : \c"
result=$($PATH 8 9 10 1 2 3 4 5 6 7 | wc -w | tr -d ' ')
	if [ "$result" -le 3 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 8 9 10 1 2 3 4 5 6 7

echo "Just RRA needed (max 1 ope) : \c"
result=$($PATH 2 3 4 5 6 7 8 9 10 1 | wc -w | tr -d ' ')
	if [ "$result" -le 1 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 2 3 4 5 6 7 8 9 10 1

echo "Just RRA RRA needed (max 2 ope): \c"
result=$($PATH 3 4 5 6 7 8 9 10 1 2 | wc -w | tr -d ' ')
	if [ "$result" -le 2 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 3 4 5 6 7 8 9 10 1 2

echo "Just RRA RRA RRA needed (max 3 ope) : \c"
result=$($PATH 4 5 6 7 8 9 10 1 2 3 | wc -w | tr -d ' ')
	if [ "$result" -le 3 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 4 5 6 7 8 9 10 1 2 3

echo "Mix RA and SA (max 2 ope) : \c"
result=$($PATH 10 2 1 3 4 5 6 7 8 9 | wc -w | tr -d ' ')
	if [ "$result" -le 2 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 10 2 1 3 4 5 6 7 8 9

echo "Mix RA and SA (max 3 ope) : \c"
result=$($PATH 9 10 2 1 3 4 5 6 7 8 | wc -w | tr -d ' ')
	if [ "$result" -le 3 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 9 10 2 1 3 4 5 6 7 8
echo "--------------------------------"
echo "\033[31;1mAlgo small stack (part 3)\033[00;0m"
echo "--------------------------------"
echo "5 powers of 10 mixed (max 12 opes) : \c"
result=$($PATH 10 50 30 20 40 | wc -w | tr -d ' ')
	if [ "$result" -le 12 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 10 50 30 20 40

echo "5 numbers with the same mix (max 12 opes) : \c"
result=$($PATH 1 5 3 2 4 | wc -w | tr -d ' ')
	if [ "$result" -le 12 ] ; then
		echo "\033[32;1mok\033[00;0m ---> \c"
	else
		echo "\033[31;1mko\033[00;0m ---> \c"
	fi
$PATH 1 5 3 2 4
echo "--------------------------------"
echo "\033[31;1mAlgo big stack\033[00;0m"
echo "--------------------------------"
echo "2000 random numbers, 250.000 is not to bad (but is it ok for you ?), 100.000 or bellow is ok : \c"
$PATH `ruby -e "puts (-1000..1000).to_a.shuffle.join(' ')"` | wc -w | tr -d ' '
